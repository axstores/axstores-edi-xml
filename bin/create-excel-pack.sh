#!/usr/bin/env bash

NAME=$(basename $0)
USAGE=$(cat<<EOF
Usage: $NAME
 - no options

Copy all XSD, XML and Excel files to the EXCEL_DIR and make a 'tar.gz' file of the directory.
XSD and XML are filtrated to correct the namespace locations.
EOF
)

if [ "$1" = "-h" ]; then 
	echo "${USAGE}"
	exit 0
fi 

BASE=$(dirname $0)
NAME=`basename $0`
ROOT=$(cd ${BASE}/.. && pwd)
cd ${ROOT}

EXCEL_DIR="excel-files"

ZIP_FILE="target/${EXCEL_DIR}.tar.gz"
rm -f ${ZIP_FILE}
tar  -czv --exclude="${EXCEL_DIR}/archive" -f ${ZIP_FILE} ${EXCEL_DIR}

ZIP_FILE="target/${EXCEL_DIR}.zip"
rm -f ${ZIP_FILE}
zip -x "${EXCEL_DIR}/archive\/*" -r ${ZIP_FILE} ${EXCEL_DIR}