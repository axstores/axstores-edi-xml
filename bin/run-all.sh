#!/usr/bin/env bash

BASE=$(dirname $0)
NAME=$(basename $0)

cd $BASE/..

mvn clean package && bin/create-specification-pack.sh && bin/update-massreg-excel-xsd-and-xml.sh && bin/create-excel-pack.sh

