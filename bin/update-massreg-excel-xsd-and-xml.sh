#!/usr/bin/env bash

NAME=$(basename $0)
USAGE=$(cat<<EOF
Usage: $NAME
 - no options

Copy all XSD, XML and Excel files to the EXCEL_DIR and make a 'tar.gz' file of the directory.
XSD and XML are filtrated to correct the namespace locations.
EOF
)

if [ "$1" = "-h" ]; then 
	echo "${USAGE}"
	exit 0
fi 

BASE=$(dirname $0)
NAME=`basename $0`
ROOT=$(cd ${BASE}/.. && pwd)
cd ${ROOT}

SCHEMAGEN_DIR=target/generated-resources/schemagen/
RESOURCE_DIR=src/test/resources

EXCEL_DIR=excel-files
EXCEL_XML_DIR=${EXCEL_DIR}/xml
EXCEL_XSD_DIR=${EXCEL_DIR}/xsd

if [ ! -d $SCHEMAGEN_DIR ]; then
	echo "Dir does not exist: $SCHEMAGEN_DIR"
	echo "run '$> mvn package' to generate directory and files needed..." 
	exit 0
fi

cp $RESOURCE_DIR/*.xml $EXCEL_XML_DIR
cp $SCHEMAGEN_DIR/*.xml  $EXCEL_XML_DIR
cp $SCHEMAGEN_DIR/*.xsd  $EXCEL_XSD_DIR

# cleanup xml
sed -i 's/\..*schemagen\// ..\/xsd\//g' ${EXCEL_XML_DIR}/*.xml
