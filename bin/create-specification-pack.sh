#!/usr/bin/env bash

BASE=$(dirname $0)
NAME=$(basename $0)
USAGE=$(cat<<EOF
Usage: $NAME
 - no options

Copy all XSD, XML and Excel files to the separate directory in 'target' and make a 'tar.gz' file of the directory.
XSD and XML are filtrated to correct the namespace locations.
EOF
)

if [ "$1" = "-h" ]; then 
	echo "${USAGE}"
	exit 0
fi 

ROOT=$(cd ${BASE}/.. && pwd)
cd ${ROOT}

#POM_GROUP=se.axstores.edi
#POM_ARTIFACT=$(mvn help:evaluate -Dexpression=project.artifactId | sed -e '/^\[/d' -e '/^\s*$/d')
#POM_VERSION=$(mvn help:evaluate -Dexpression=project.version | sed -e '/^\[/d' -e '/^\s*$/d' -e 's/-SNAPSHOT//')

TMP_DIR="target/xml-specifications-temp"
ZIP_DIR_NAME="edi-pricelist-xml"
WRK_DIR="${TMP_DIR}/${ZIP_DIR_NAME}"
ZIP_FILE_NAME=../${ZIP_DIR_NAME}

#/tmp/`basename $0/`1.$$
#trap "{ rm -f $TMP_DIR }" EXIt
#mvn clean package

rm -rf ${TMP_DIR}
mkdir -p ${WRK_DIR}

#
cp src/test/resources/*.xml ${WRK_DIR}

cp target/generated-resources/schemagen/*.xsd  ${WRK_DIR}

cp target/generated-resources/schemagen/*.xml  ${WRK_DIR}
#cd $TMP_DIR/axstores-edi-xml

# cleanup xml
for XML in ${WRK_DIR}/*.xml ; do
	echo "Process file: '${XML}'"
    sed -i -e 's/\..*schemagen\// /g' ${XML}
done

(cd ${TMP_DIR} && tar cfz ${ZIP_FILE_NAME}.tar.gz ${ZIP_DIR_NAME} && zip -r ${ZIP_FILE_NAME}.zip ${ZIP_DIR_NAME} )
