package se.axstores.edi.v1.model.pricelist;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.JAXBException;

import org.apache.commons.validator.routines.checkdigit.CheckDigitException;
import org.junit.Test;
import org.xml.sax.SAXException;

public class PriceListTest {

	@Test
	public void testReadPriceListNoValidate() throws JAXBException, SAXException {
		String xmlFileName = "edi-pricelist-v1.0.xml";
		PriceList priceList = PriceListXmlHelper.readPriceListXml(xmlFileName, false);
		assertNotNull(priceList);
	}

	@Test
	public void testReadPriceList() throws JAXBException, SAXException {
		String xmlFileName = "edi-pricelist-v1.0.xml";
		PriceList priceList = PriceListXmlHelper.readPriceListXml(xmlFileName, true);
		assertNotNull(priceList);
	}

	@Test
	public void testReadPriceListValidateData() throws JAXBException, SAXException, ParseException {
		DateTimeFormatter dateFormater = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String xmlFileName = "edi-pricelist-v1.0.xml";
		PriceList priceList = PriceListXmlHelper.readPriceListXml(xmlFileName, true);
		assertNotNull(priceList);
		// Header
		assertEquals("SUPPLIER GLN", priceList.getPriceListHeader().getSupplier().getId().getValue());
		assertEquals("7310640005994", priceList.getPriceListHeader().getBuyer().getId().getValue());
		assertEquals("Autumn Winter 2018", priceList.getPriceListHeader().getPriceListName());
		assertEquals(LocalDate.parse("2017-01-01", dateFormater), priceList.getPriceListHeader().getPriceListPeriod().from);
		assertEquals(LocalDate.parse("2017-12-31"), priceList.getPriceListHeader().getPriceListPeriod().to);
		assertEquals("SEK", priceList.getPriceListHeader().getDefaultCurrency());
		// Items
		assertEquals(1, priceList.getItemList().size());
		Item item1 = priceList.getItemList().get(0);

		assertEquals("1234567890123", item1.getId().getValue());
		assertEquals("9876543", item1.getArticleVpn().getValue());
		assertEquals("9876543-128", item1.getItemVpn().getValue());

		assertEquals("1234567890123", item1.getBarcodeList().get(0).getValue());

		assertEquals("3926206000", item1.getHtsCode().getValue());

		assertEquals("Brand name", item1.getBrand());
		assertEquals("Function", item1.getFunction());
		assertEquals("Series name", item1.getSeries());
		assertEquals("Article name", item1.getName());

		assertEquals("90", item1.getVendorColor().getCode());
		assertEquals("BLACK", item1.getVendorColor().getValue());
		assertEquals("C75", item1.getVendorSize().getValue());

		assertEquals(new Integer(250), item1.getItemDimensions().getHeight());
		assertEquals(new Integer(100), item1.getItemDimensions().getLength());
		assertEquals(new Integer(95), item1.getItemDimensions().getWidth());

		assertEquals(new Integer(1), item1.getPackingInfo().getInnerPackSize());
		assertEquals(new Integer(1), item1.getPackingInfo().getCaseSize());

		assertEquals("DK", item1.getCountryOfManufacturing());

		assertEquals(LocalDate.parse("2016-10-01"), item1.getOrderPeriod().from);
		assertEquals(LocalDate.parse("2017-11-01"), item1.getOrderPeriod().to);

		assertEquals(null, item1.getDeliveryPeriod().from);
		assertEquals(LocalDate.parse("2017-12-01"), item1.getDeliveryPeriod().to);

		assertEquals(LocalDate.parse("2017-02-15"), item1.getSalesPeriod().from);
		assertEquals(null, item1.getSalesPeriod().to);

		assertEquals(null, item1.getAvailabilityPeriod().from);
		assertEquals(LocalDate	.parse("2019-12-31"), item1.getAvailabilityPeriod().to);

		assertEquals("AW17", item1.getSeason());
		assertEquals("Xmas", item1.getPhase());

		assertEquals(new BigDecimal("100.0000"), item1.getCostPriceInfo().getListPrice());
		assertEquals(new BigDecimal("1.25"), item1.getCostPriceInfo().getAllowancePercent());
		assertEquals(new BigDecimal("98.7500"), item1.getCostPriceInfo().getNetPrice());

		assertEquals(new BigDecimal("699.0000"), item1.getRetailPriceInfoSEK().getPrice());

		assertEquals("ADR", item1.getHazardousGoodsInfo().getRegulationCode());
		assertEquals("3B", item1.getHazardousGoodsInfo().getHazardCode());
		assertEquals("1178", item1.getHazardousGoodsInfo().getUnNumber());
		assertEquals(se.axstores.edi.v1.model.common.TemperatureUnit.C,
				item1.getHazardousGoodsInfo().getFlashpointTemp().getUnit());
		assertEquals(new BigDecimal("21.0000"), item1.getHazardousGoodsInfo().getFlashpointTemp().getValue());
		assertEquals("2", item1.getHazardousGoodsInfo().getDangerLevelCode());
		assertEquals("Special Packaging", item1.getHazardousGoodsInfo().getPackingInstruction().getValue());

	}

	@Test
	public void testGeneratePriceList() throws JAXBException, SAXException, ParseException, CheckDigitException {

		String priceListName = "Auto generated price list for TEST";
		String supplierIdEAN = "731064000001";
		String buyerIdEAN = "731064000002";
		String pricelistValidFrom = "2017-01-31";
		String pricelistValidTo = "2017-07-31";
		int nbItems = 1000;
		PriceList priceList = PriceListBuilder.newPriceList(priceListName, supplierIdEAN, buyerIdEAN,
				pricelistValidFrom, pricelistValidTo, nbItems);
		String itemXmlString = PriceListXmlHelper.priceListToXmlString(priceList);
		assertNotNull(itemXmlString);
		assertTrue(0 < priceList.getItemList().get(0).getCostPriceInfo().getNetPrice().intValue());
		File file = new File("target/generated-resources/schemagen/auto-generated-pricelist.xml");
		System.out.println("Writing " + priceList.getItemList().size() + " items to file: " + file);
		PriceListXmlHelper.writePriceList(priceList, file);
	}
}
