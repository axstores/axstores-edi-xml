package se.axstores.edi.v1.model.pricelist;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

public class PriceListXmlHelper {

	public static PriceList readPriceListXml(String xmlFileName, boolean validate) throws JAXBException, SAXException {
		ClassLoader classLoader = getContextClassLoader();
		File file = new File(classLoader.getResource(xmlFileName).getFile());
		JAXBContext jaxbContext = JAXBContext.newInstance(PriceList.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		if (validate) {
			SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			String schemaFileName = "edi-pricelist-v1.0.xsd";
			File schemaFile = new File(classLoader.getResource(schemaFileName).getFile());
			Schema schema = sf.newSchema(schemaFile);
			jaxbUnmarshaller.setSchema(schema);

			jaxbUnmarshaller.setEventHandler(new ValidationEventHandler() {

				@Override
				public boolean handleEvent(ValidationEvent validationEvent) {
					String severityMessage = "OK";
					int severity = validationEvent.getSeverity();
					switch(severity) {
					case ValidationEvent.WARNING:
						severityMessage = "WARNING";
						break;
					case ValidationEvent.ERROR:
						severityMessage = "WARNING";
						break;
					case ValidationEvent.FATAL_ERROR:
						severityMessage = "FATAL_ERROR";
						break;
					}
					System.out.println(severityMessage + " file=" + xmlFileName + ", line=" + validationEvent.getLocator().getLineNumber() //
							+ ", col=" + validationEvent.getLocator().getColumnNumber() //
							+ ", message=" + validationEvent.getMessage());
					
					return severity != ValidationEvent.FATAL_ERROR ? false : false;
				}

			});
		}

		PriceList priceList = (PriceList) jaxbUnmarshaller.unmarshal(file);
		return priceList;
	}

	public static void writePriceList(PriceList priceList, File file) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(PriceList.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(priceList, file);
	}

	public static String priceListToXmlString(PriceList priceList) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(PriceList.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(priceList, sw);
		return sw.toString();
	}

	public static List<String> getResourceFiles(String path) throws IOException {
		List<String> filenames = new ArrayList<>();

		try (InputStream in = getResourceAsStream(path);
				BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
			String resource;

			while ((resource = br.readLine()) != null) {
				filenames.add(resource);
			}
		}

		return filenames;
	}

	public static InputStream getResourceAsStream(String resource) {
		final InputStream in = getContextClassLoader().getResourceAsStream(resource);

		return in == null ? PriceListXmlHelper.class.getResourceAsStream(resource) : in;
	}

	private static ClassLoader getContextClassLoader() {
		return Thread.currentThread().getContextClassLoader();
	}
}
