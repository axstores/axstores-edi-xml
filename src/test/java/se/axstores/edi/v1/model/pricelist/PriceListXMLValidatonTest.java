	package se.axstores.edi.v1.model.pricelist;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Test;
import org.xml.sax.SAXException;

public class PriceListXMLValidatonTest {
		
	@Test
	public void verifyTestXmlFiles() throws IOException {
		String resourcePath = "test-xml";
		List<String> xmlFileNames = PriceListXmlHelper.getResourceFiles(resourcePath);
		for(String xmlFileName : xmlFileNames) {
			if(xmlFileName.startsWith("err-") || !xmlFileName.endsWith(".xml")) {
				continue;
			}
			PriceList priceList;
			try {
				priceList = PriceListXmlHelper.readPriceListXml(resourcePath + "/" + xmlFileName, false);
				assertNotNull(priceList);
				priceList = PriceListXmlHelper.readPriceListXml(resourcePath + "/" + xmlFileName, true);
				assertNotNull(priceList);
			} catch (JAXBException e) {	
				System.out.println(e.toString().replaceAll("\n", ""));
			} catch (SAXException e) {
				System.out.println(e.toString().replaceAll("\n", ""));
			}
		}
	}
}

