package se.axstores.edi.v1.model.pricelist;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.UUID;

import org.apache.commons.validator.routines.checkdigit.CheckDigitException;
import org.apache.commons.validator.routines.checkdigit.EAN13CheckDigit;

import se.axstores.edi.v1.model.common.CodeValue;
import se.axstores.edi.v1.model.common.ContactInfo;
import se.axstores.edi.v1.model.common.DimensionUnit;
import se.axstores.edi.v1.model.common.Dimensions;
import se.axstores.edi.v1.model.common.Identifier;
import se.axstores.edi.v1.model.common.Period;
import se.axstores.edi.v1.model.common.Temperature;
import se.axstores.edi.v1.model.common.TypedValue;
import se.axstores.edi.v1.model.common.Volume;
import se.axstores.edi.v1.model.common.Weight;

public class PriceListBuilder {

	public static PriceList newPriceList(String priceListName, String supplierIdEAN, String buyerIdEAN,
			String pricelistValidFrom, String pricelistValidTo, int nbItems)
			throws ParseException, CheckDigitException {
		PriceListBuilder builder = new PriceListBuilder();
		PriceList priceList = builder.createPriceList(priceListName, supplierIdEAN, buyerIdEAN, pricelistValidFrom,
				pricelistValidTo);
		String[] sizes = { "XXS", "XS", "S", "M", "L", "XL", "XXL" };
		String[] colors = { "Red", "Green", "Blue", "Black", "Purple", "Beige", "Amber" };
		builder.createItems(priceList, "Tst Brnd", "Tst Prd", "Tst Fun", nbItems, sizes, colors);
		return priceList;
	}

	private void createItems(PriceList priceList, String brand, String series, String function, int nbItems,
			String[] sizes, String[] colors) throws CheckDigitException {
		int[] cosePrices = { 100, 200, 300, 400, 500 };
		String[] retailPriceCurrency = { "SEK", "EUR" };
		int[] retailPrices = { 399, 799, 1199, 1599, 1999 };
		int itemCount = 0;
		for (int ii = 1; ii <= nbItems; ii++) {
			itemCount++;
			int sizeCount = 0;
			for (String size : sizes) {
				sizeCount++;
				int colorCount = 0;
				for (String color : colors) {
					colorCount++;

					Item item = new Item();
					priceList.getItemList().add(item);

					String itemCode = String.format("%04d", itemCount);
					String sizeCode = String.format("%03d", sizeCount);
					String colorCode = String.format("%03d", colorCount);

					String itemId = itemCode + sizeCode + colorCode;
					item.setId(new Identifier());
					item.getId().setType(se.axstores.edi.v1.model.common.IdentifierType.OTHER);
					item.getId().setValue(itemId);
					item.setArticleVpn(item.getId());
					item.setItemVpn(new Identifier());
					item.getItemVpn().setType(se.axstores.edi.v1.model.common.IdentifierType.OTHER);
					item.getItemVpn().setValue(itemId + "-" + size + "-" + color);
					item.setBarcodeList(new ArrayList<>());
					Identifier barcode = new Identifier();
					barcode.setType(se.axstores.edi.v1.model.common.IdentifierType.EAN);
					barcode.setValue(generateEAN13("73" + itemId));
					item.getBarcodeList().add(barcode);
					item.setHtsCode(new TypedValue("HTS", "8523.49.2010"));
					item.setSustainabilityInfo(new SustainabilityInfo());
					item.getSustainabilityInfo().setCertificate("FSC 100");
					item.setBrand(brand);
					item.setSeries(series);
					item.setVendorSize(new TypedValue("EU", size));
					item.setVendorColor(new CodeValue(colorCode, color));
					item.setItemDimensions(createDimensions(DimensionUnit.CM, ii, 2 * ii, 3 * ii));
					item.setPackingInfo(createPackingInfo(1, 1));
					item.setCountryOfManufacturing("SE");
					item.setCostPriceInfo(new CostPriceInfo());
					item.getCostPriceInfo().setListPrice(new BigDecimal(cosePrices[itemCount % cosePrices.length]));
					item.getCostPriceInfo().setAllowancePercent(new BigDecimal(15));
					item.getCostPriceInfo().setNetPrice(calculateReducedPrice(item.getCostPriceInfo().getListPrice(),
							item.getCostPriceInfo().getAllowancePercent()));
					item.setRetailPriceInfoSEK(new RetailPriceInfo());
					item.getRetailPriceInfoSEK().setCurrency(retailPriceCurrency[0]);
					item.getRetailPriceInfoSEK()
							.setPrice(new BigDecimal(retailPrices[itemCount % retailPrices.length]));
					item.getRetailPriceInfoSEK().setPrice(
							calculateReducedPrice(item.getRetailPriceInfoSEK().getPrice(), new BigDecimal(10.0)));
					item.setHazardousGoodsInfo(new HazardousGoodsInfo());
					// item.hazardousGoodsInfo.regulationCode = "ADR";
					item.getHazardousGoodsInfo().setHazardCode("3B");
					item.getHazardousGoodsInfo().setUnNumber("1178");
					item.getHazardousGoodsInfo().setDangerLevelCode("2");
					item.getHazardousGoodsInfo().setFlashpointTemp(
							createTemperature(se.axstores.edi.v1.model.common.TemperatureUnit.C, new BigDecimal(21)));
					item.getHazardousGoodsInfo().setNetVolume(
							createVolume(se.axstores.edi.v1.model.common.VolumeUnit.ML, new BigDecimal(250)));
					item.getHazardousGoodsInfo()
					
							.setNetWeight(createWeight(se.axstores.edi.v1.model.common.WeightUnit.G, 150));
					item.getHazardousGoodsInfo().setPackingInstruction(createTypedValue("Special Packaging"));
				}
			}
		}
	}

	private PackingInfo createPackingInfo(int innerPackSize, int caseSize) {
		PackingInfo p = new PackingInfo(innerPackSize, caseSize);
		return p;
	}

	private Dimensions createDimensions(DimensionUnit unit, int height, int length, int width) {
		Dimensions d = new Dimensions(unit, height, length, width);
		return d;
	}

	private TypedValue createTypedValue(String value) {
		return createTypedValue(null, value);
	}

	private TypedValue createTypedValue(String type, String value) {
		TypedValue tp = new TypedValue(type, value);
		return tp;
	}	

	private Temperature createTemperature(se.axstores.edi.v1.model.common.TemperatureUnit unit, BigDecimal value) {
		Temperature t = new Temperature(unit, value);
		return t;
	}

	private Volume createVolume(se.axstores.edi.v1.model.common.VolumeUnit unit, BigDecimal value) {
		Volume v = new Volume(unit, value);
		return v;
	}

	private Weight createWeight(se.axstores.edi.v1.model.common.WeightUnit unit, Integer value) {
		Weight w = new Weight(unit, value);
		return w;
	}

	private BigDecimal calculateReducedPrice(BigDecimal price, BigDecimal discountPercent) {
		BigDecimal discount = price.multiply(discountPercent.divide(new BigDecimal(100)));
		BigDecimal discountPrice = price.subtract(discount);
		return discountPrice;
	}

	private String generateEAN13(String itemId) throws CheckDigitException {
		assert (itemId.length() == 12);
		EAN13CheckDigit ean13CheckDigit = new EAN13CheckDigit();
		String checkdigit = ean13CheckDigit.calculate(itemId);
		return itemId + checkdigit;
	}

	private PriceList createPriceList(String priceLIstName, String suppilerId, String buyerId, String fromDate,
			String toDate) throws ParseException {
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		PriceList priceList = new PriceList();
		priceList.setPriceListHeader(new PriceListHeader());
		priceList.setMessageId(UUID.randomUUID().toString());
		priceList.setMessageTimestamp(LocalDateTime.now());
		priceList.getPriceListHeader().setSupplier(
				new ContactInfo(new Identifier(se.axstores.edi.v1.model.common.IdentifierType.GLN, suppilerId)));
		priceList.getPriceListHeader()
				.setBuyer(new ContactInfo(new Identifier(se.axstores.edi.v1.model.common.IdentifierType.GLN, buyerId)));
		priceList.getPriceListHeader().setPriceListName(priceLIstName);
		priceList.getPriceListHeader().setPriceListPeriod(new Period(fromDate, toDate));
		priceList.getPriceListHeader().setDefaultCurrency("SEK");
		priceList.setItemList(new ArrayList<>());
		return priceList;
	}
}