#!/bin/sh
#
# Make commented edi file easy to import in Excel.
#
CSV_FILE=pricat-mig-example.csv
echo "EDI segment;Comment;XML mapping" > $CSV_FILE
sed -e 's/\s*#\s*/;/' -e 's/\s*\[XPath:/;/' -e 's/\]\s*$//' pricat-mig-example.edi >> $CSV_FILE