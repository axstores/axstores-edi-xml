package se.axstores.edi.util.xmladapter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class IsoDateAdapter extends XmlAdapter<String, LocalDate> {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");//.withZone(ZoneId.of("UTC"));

    @Override
    public String marshal(LocalDate v) throws Exception {
        return FORMATTER.format(v);
    }

    @Override		
    public LocalDate unmarshal(String v) throws Exception {
        return LocalDate.parse(v, FORMATTER);
    }

}