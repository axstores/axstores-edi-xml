package se.axstores.edi.util.xmladapter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class IsoDateTimeAdapter extends XmlAdapter<String, LocalDateTime> {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");//[X][XXX]").withZone(ZoneId.of("UTC"));
    @Override
    public String marshal(LocalDateTime v) throws Exception {
        return FORMATTER.format(v);
    }

    @Override
    public LocalDateTime unmarshal(String v) throws Exception {
        return LocalDateTime.parse(v, FORMATTER);
    }
}	