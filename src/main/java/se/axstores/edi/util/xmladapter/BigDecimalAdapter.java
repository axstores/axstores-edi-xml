package se.axstores.edi.util.xmladapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigDecimalAdapter extends XmlAdapter<String, BigDecimal> {

    @Override
    public String marshal(BigDecimal value) throws Exception {
        if (value != null) {
            value = value.setScale(2, RoundingMode.HALF_EVEN);
            return value.toString();
        }
        return null;
    }

    @Override
    public BigDecimal unmarshal(String str) throws Exception {
        BigDecimal value = new BigDecimal(str);
        value = value.setScale(4, RoundingMode.HALF_UP);
        return value;
    }
}