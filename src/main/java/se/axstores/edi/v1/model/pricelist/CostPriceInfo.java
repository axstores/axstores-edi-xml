package se.axstores.edi.v1.model.pricelist;

import se.axstores.edi.util.xmladapter.BigDecimalAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;

/**
 * Specify the item cost price, allowance and net price
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-pricelist-v1.0")
public class CostPriceInfo {

    /**
     * Suppliers list price, currency specified by supplier configuration
     * <p>
     * N.B., currency in price list header only used for validation.
     */
    @XmlElement
    @XmlJavaTypeAdapter(BigDecimalAdapter.class)
    private BigDecimal listPrice;

    /**
     * Buyers allowance on the item
     */
    @XmlElement
    private BigDecimal allowancePercent;

    /**
     * Buyers price calculated, cost - allowance
     */
    @XmlElement
    @XmlJavaTypeAdapter(BigDecimalAdapter.class)
    private BigDecimal netPrice;

    public BigDecimal getListPrice() {
        return listPrice;
    }

    public void setListPrice(BigDecimal listPrice) {
        this.listPrice = listPrice;
    }

    public BigDecimal getAllowancePercent() {
        return allowancePercent;
    }

    public void setAllowancePercent(BigDecimal allowancePercent) {
        this.allowancePercent = allowancePercent;
    }

    public BigDecimal getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(BigDecimal netPrice) {
        this.netPrice = netPrice;
    }
}
