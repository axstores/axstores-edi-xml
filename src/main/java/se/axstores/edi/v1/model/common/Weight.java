package se.axstores.edi.v1.model.common;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-common-v1.0")
public class Weight {

    @XmlAttribute(required = true)
    private WeightUnit unit;

    @XmlValue
    private Integer value;
    public Weight() { }

    public Weight(WeightUnit unit, Integer value) {
    	setUnit(unit);
    	setValue(value);
    }

	public WeightUnit getUnit() {
        return unit;
    }

    public void setUnit(WeightUnit unit) {
        this.unit = unit;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
