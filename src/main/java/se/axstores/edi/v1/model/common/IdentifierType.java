package se.axstores.edi.v1.model.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType(namespace = "uri:axstores-edi-xml:edi-common-v1.0")
@XmlEnum
public enum IdentifierType {
    EAN, GLN, GTN, OTHER;

    public static IdentifierType fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }
}
