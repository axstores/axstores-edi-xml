package se.axstores.edi.v1.model.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(namespace = "uri:axstores-edi-xml:edi-common-v1.0")
@XmlEnum
public enum WeightUnit {
	@XmlEnumValue("g") G("g"),
	@XmlEnumValue ("kg") KG("kg"),
	@XmlEnumValue ("ton") TON("ton"),
	@XmlEnumValue ("oz") OZ("oz"),
	@XmlEnumValue ("lbs") LBS("lbs");

	private final String val;

	WeightUnit(String val) {
		this.val = val;
	}

	@Override
	public String toString() {
		return name().toLowerCase();
	}

	public String value(){
		return val;
	}


	public static WeightUnit fromValue(String value) {
		for(WeightUnit e: WeightUnit.values()) {
			if(e.val.equalsIgnoreCase(value)) {
				return e;
			}
		}
		return null;// not found
	}

}
