package se.axstores.edi.v1.model.pricelist;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Item packing information
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-pricelist-v1.0")
public class PackingInfo {

	public PackingInfo() {
	}

	public PackingInfo(Integer innerPackSize, Integer caseSize) {
		super();
		this.innerPackSize = innerPackSize;
		this.caseSize = caseSize;
	}

	/**
	 * Consumer sellable unit - K-pack
	 */
	@XmlElement
	private Integer innerPackSize;

	/**
	 * Store ordering unit - B-pack
	 */
	@XmlElement
	private Integer caseSize;
	// public Integer unitsInStorePack;

	/**
	 * DC/WH ordering unit - T-pack.
	 * <p>
	 * Unknown usage.
	 */

	public Integer getInnerPackSize() {
		return innerPackSize;
	}

	public void setInnerPackSize(Integer innerPackSize) {
		this.innerPackSize = innerPackSize;
	}

	public Integer getCaseSize() {
		return caseSize;
	}

	public void setCaseSize(Integer caseSize) {
		this.caseSize = caseSize;
	}
}
