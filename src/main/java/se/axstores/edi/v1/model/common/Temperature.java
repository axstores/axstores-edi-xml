package se.axstores.edi.v1.model.common;

import se.axstores.edi.util.xmladapter.BigDecimalAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-common-v1.0")
public class Temperature {

	/**
	 * If null, then C is assumed as default value.
	 */
	@XmlAttribute(required = false)
	private TemperatureUnit unit;

	@XmlValue
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal value;

	public Temperature() {
	}

	public Temperature(TemperatureUnit unit, BigDecimal value) {
		setUnit(unit);
		setValue(value);
	}

	public TemperatureUnit getUnit() {
		return unit;
	}

	public void setUnit(TemperatureUnit unit) {
		this.unit = unit;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}
}
