package se.axstores.edi.v1.model.common;

import java.time.LocalDate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import se.axstores.edi.util.xmladapter.IsoDateAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-common-v1.0")
public class Period {

	public Period() {
		super();
	}

	public Period(LocalDate from, LocalDate to) {
		super();
		this.from = from;
		this.to = to;
	}

	public Period(String from, String to) {
		this(LocalDate.parse(from), LocalDate.parse(to));
	}

	@XmlElement
	@XmlJavaTypeAdapter(IsoDateAdapter.class)
	private LocalDate from;

	@XmlElement
	@XmlJavaTypeAdapter(IsoDateAdapter.class)
	private LocalDate to;


	public LocalDate getFrom() {
		return from;
	}

	public void setFrom(LocalDate from) {
		this.from = from;
	}

	public LocalDate getTo() {
		return to;
	}

	public void setTo(LocalDate to) {
		this.to = to;
	}

}
