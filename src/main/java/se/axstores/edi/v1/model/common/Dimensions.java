package se.axstores.edi.v1.model.common;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-common-v1.0")
public class Dimensions {

	public Dimensions() {

	}

	public Dimensions(DimensionUnit unit, Integer height, Integer length, Integer width) {
		super();
		this.unit = unit;
		this.height = height;
		this.length = length;
		this.width = width;
	}

	@XmlAttribute(required = true)
	public DimensionUnit unit;

	@XmlElement
	private Integer height;

	@XmlElement
	private Integer length;

	@XmlElement
	private Integer width;

	public DimensionUnit getUnit() {
		return unit;
	}

	public void setUnit(DimensionUnit unit) {
		this.unit = unit;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}
}
