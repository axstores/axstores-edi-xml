package se.axstores.edi.v1.model.common;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-common-v1.0")
public class TypedValue {

	@XmlAttribute
	private String type;

	@XmlValue
	private String value;

	public TypedValue() {
	}

	public TypedValue(String type, String value) {
		setType(type);
		setValue(value);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
