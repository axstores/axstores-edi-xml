package se.axstores.edi.v1.model.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(namespace = "uri:axstores-edi-xml:edi-common-v1.0")
@XmlEnum
public enum VolumeUnit {
	@XmlEnumValue ("ml") ML("ml"),
	@XmlEnumValue ("cl") CL("cl"),
	@XmlEnumValue ("l") L("l") ,
	@XmlEnumValue ("m3") M3("m3");

	private final String val;

	VolumeUnit(String val) {
		this.val = val;
	}

	@Override
	public String toString() {
		return name().toLowerCase();
	}

	public String value(){
		return val;
	}


	public static VolumeUnit fromValue(String value) {
		for(VolumeUnit e: VolumeUnit.values()) {
			if(e.val.equalsIgnoreCase(value)) {
				return e;
			}
		}
		return null;// not found
	}

}
