package se.axstores.edi.v1.model.pricelist;

import se.axstores.edi.v1.model.common.EdiMessage;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Axstores price list EDI XML format
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "PriceList", namespace = "uri:axstores-edi-xml:edi-pricelist-v1.0")
@XmlType(namespace = "uri:axstores-edi-xml:edi-pricelist-v1.0")
public class PriceList extends EdiMessage {

    /**
     * Price list header.
     * <p>
     * General info about supplier, buyer, dates and other general and default values.
     */
    @XmlElement(name = "header", required = true)
    private PriceListHeader priceListHeader;

    /**
     * The items
     */
    @XmlElementWrapper(name = "itemList", required = true)
    @XmlElement(name = "item")
    private List<Item> itemList;


    public PriceListHeader getPriceListHeader() {
        return priceListHeader;
    }

    public void setPriceListHeader(PriceListHeader priceListHeader) {
        this.priceListHeader = priceListHeader;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }
}
