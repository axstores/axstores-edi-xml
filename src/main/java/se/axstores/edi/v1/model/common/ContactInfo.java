package se.axstores.edi.v1.model.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-common-v1.0")
public class ContactInfo {

	public ContactInfo() {
		super();
	}

	public ContactInfo(Identifier id) {
		super();
		this.id = id;
	}

	public ContactInfo(Identifier id, String contact, Address address, String emailAddress, String telephoneNumber,
			String faxNumber) {
		super();
		this.id = id;
		this.contact = contact;
		this.address = address;
		this.emailAddress = emailAddress;
		this.telephoneNumber = telephoneNumber;
		this.faxNumber = faxNumber;
	}

	@XmlElement(required = true)
	private Identifier id;

	@XmlElement
	private String contact;

	@XmlElement
	private Address address;

	@XmlElement
	private String emailAddress;

	@XmlElement
	private String telephoneNumber;

	@XmlElement
	private String faxNumber;

	public Identifier getId() {
		return id;
	}

	public void setId(Identifier id) {
		this.id = id;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

}