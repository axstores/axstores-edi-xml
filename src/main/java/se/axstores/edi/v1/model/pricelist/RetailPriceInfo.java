package se.axstores.edi.v1.model.pricelist;

import se.axstores.edi.util.xmladapter.BigDecimalAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-pricelist-v1.0")
public class RetailPriceInfo {

    /**
     * If not specified - then the defaultCurrency is used
     */
    @XmlElement(required = false)
    private String currency;

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(BigDecimalAdapter.class)
    private BigDecimal price;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
