package se.axstores.edi.v1.model.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType(namespace = "uri:axstores-edi-xml:edi-common-v1.0")
@XmlEnum
public enum TemperatureUnit {
    C("c"), F("f"), K("k");

    private final String val;

    TemperatureUnit(String val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return name().toLowerCase();
    }

    public String value(){
        return val;
    }

    public static TemperatureUnit fromValue(String value) {
        for(TemperatureUnit e: TemperatureUnit.values()) {
            if(e.val.equalsIgnoreCase(value)) {
                return e;
            }
        }
        return null;// not found
    }
}