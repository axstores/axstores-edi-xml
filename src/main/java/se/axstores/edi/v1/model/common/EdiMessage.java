package se.axstores.edi.v1.model.common;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import se.axstores.edi.util.xmladapter.IsoDateTimeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-common-v1.0")
public class EdiMessage {

	@XmlElement(required = true)
	private String messageVersion;

	@XmlElement(required = true)
	@XmlJavaTypeAdapter(IsoDateTimeAdapter.class)
	private LocalDateTime messageTimestamp;

	@XmlElement(required = true)
	private String messageId;

	public String getMessageVersion() {
		return messageVersion;
	}

	public void setMessageVersion(String messageVersion) {
		this.messageVersion = messageVersion;
	}

	public LocalDateTime getMessageTimestamp() {
		return messageTimestamp;
	}

	public void setMessageTimestamp(LocalDateTime messageTimestamp) {
		this.messageTimestamp = messageTimestamp;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
}
