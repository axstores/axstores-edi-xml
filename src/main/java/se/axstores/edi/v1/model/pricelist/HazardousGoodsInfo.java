package se.axstores.edi.v1.model.pricelist;

import se.axstores.edi.v1.model.common.Temperature;
import se.axstores.edi.v1.model.common.TypedValue;
import se.axstores.edi.v1.model.common.Volume;
import se.axstores.edi.v1.model.common.Weight;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Hazardous goods information.
 * <p>
 * Maps from HAN
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-pricelist-v1.0")
public class HazardousGoodsInfo {

    // ADR/RID vs IMDG vs IATA
    /**
     * Maps from DGS - code list 8273
     */
    @XmlElement
    private String regulationCode;

    /**
     * Maps from DGS - code list 8273
     */
    @XmlElement
    private String hazardCode;

    /**
     * Maps from DGS - UNDG INFORMATION / United Nations Dangerous Goods (UNDG)
     * identifier
     */
    @XmlElement
    private String unNumber;

    /**
     * Maps from DGS - UNDG INFORMATION / Dangerous goods flashpoint value
     */
    @XmlElement
    private Temperature flashpointTemp;

    /**
     * Maps from DGS - PACKAGING DANGER LEVEL CODE
     */
    @XmlElement
    private String dangerLevelCode;


    /**
     * Maps from DGS/QTY - QUANTITY DETAILS
     */
    @XmlElement
    private Volume netVolume;

    /**
     * Maps from DGS/QTY - QUANTITY DETAILS
     */
    @XmlElement
    private Weight netWeight;

    /**
     * Maps from DGS/FTX 'qualifier = PAC' Free text literal value
     */
    @XmlElement
    private TypedValue packingInstruction;

    @XmlElement
    private String tunnelRestrictions;

    @XmlElement
    private String specialPacking;

    @XmlElement
    private String classNumeric;

    public String getRegulationCode() {
        return regulationCode;
    }

    public void setRegulationCode(String regulationCode) {
        this.regulationCode = regulationCode;
    }

    public String getHazardCode() {
        return hazardCode;
    }

    public void setHazardCode(String hazardCode) {
        this.hazardCode = hazardCode;
    }

    public String getUnNumber() {
        return unNumber;
    }

    public void setUnNumber(String unNumber) {
        this.unNumber = unNumber;
    }

    public Temperature getFlashpointTemp() {
        return flashpointTemp;
    }

    public void setFlashpointTemp(Temperature flashpointTemp) {
        this.flashpointTemp = flashpointTemp;
    }

    public String getDangerLevelCode() {
        return dangerLevelCode;
    }

    public void setDangerLevelCode(String dangerLevelCode) {
        this.dangerLevelCode = dangerLevelCode;
    }

    public Volume getNetVolume() {
        return netVolume;
    }

    public void setNetVolume(Volume netVolume) {
        this.netVolume = netVolume;
    }

    public Weight getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(Weight netWeight) {
        this.netWeight = netWeight;
    }

    public TypedValue getPackingInstruction() {
        return packingInstruction;
    }

    public void setPackingInstruction(TypedValue packingInstruction) {
        this.packingInstruction = packingInstruction;
    }

    public String getTunnelRestrictions() {
        return tunnelRestrictions;
    }

    public void setTunnelRestrictions(String tunnelRestrictions) {
        this.tunnelRestrictions = tunnelRestrictions;
    }

    public String getSpecialPacking() {
        return specialPacking;
    }

    public void setSpecialPacking(String specialPacking) {
        this.specialPacking = specialPacking;
    }

    public String getClassNumeric() {
        return classNumeric;
    }

    public void setClassNumeric(String classNumeric) {
        this.classNumeric = classNumeric;
    }
}
