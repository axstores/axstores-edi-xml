package se.axstores.edi.v1.model.common;

import se.axstores.edi.util.xmladapter.BigDecimalAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-common-v1.0")
public class Volume {

	@XmlAttribute(required = true)
	private VolumeUnit unit;

	@XmlValue
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal value;

	public Volume() {}

	public Volume(VolumeUnit unit, BigDecimal value) {
		setUnit(unit);
		setValue(value);
	}

	public VolumeUnit getUnit() {
		return unit;
	}

	public void setUnit(VolumeUnit unit) {
		this.unit = unit;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

}
