package se.axstores.edi.v1.model.pricelist;

import se.axstores.edi.util.xmladapter.BigDecimalAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;
//import se.axstores.edi.v1.model.common.Period;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-pricelist-v1.0")
public class ItemPrice {

    @XmlElement(required = true)
    private String currency;

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(BigDecimalAdapter.class)
    private BigDecimal amount;


    @XmlElement
    @XmlJavaTypeAdapter(BigDecimalAdapter.class)
    private BigDecimal discountAmount;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }
}
