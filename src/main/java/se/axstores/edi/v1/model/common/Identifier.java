package se.axstores.edi.v1.model.common;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-common-v1.0")
public class Identifier {

		
    public Identifier() {
		super();
	}

	public Identifier(IdentifierType type, String value) {
		super();
		this.type = type;
		this.value = value;
	}

	@XmlAttribute(required = false)
    private IdentifierType type;

    @XmlValue
    private String value;

    public IdentifierType getType() {
        return type;
    }

    public void setType(IdentifierType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
