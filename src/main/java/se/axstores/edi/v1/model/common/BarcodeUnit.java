package se.axstores.edi.v1.model.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType(namespace = "uri:axstores-edi-xml:edi-common-v1.0")
@XmlEnum
public enum BarcodeUnit {
    EAN, OTHER;

    public static BarcodeUnit fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }
}
