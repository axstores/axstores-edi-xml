package se.axstores.edi.v1.model.common;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-common-v1.0")
public class CodeValue {

	@XmlAttribute(required = false)
	private String code;

	public CodeValue() {
		super();
	}

	public CodeValue(String code, String value) {
		super();
		this.code = code;
		this.value = value;
	}

	@XmlValue
	private String value;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
