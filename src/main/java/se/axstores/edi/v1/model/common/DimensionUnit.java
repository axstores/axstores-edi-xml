package se.axstores.edi.v1.model.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlType(namespace = "uri:axstores-edi-xml:edi-common-v1.0")
@XmlEnum
public enum DimensionUnit {
	@XmlEnumValue ("mm") MM("mm"),
	@XmlEnumValue("cm") CM("cm"),
	@XmlEnumValue("m") M("m");

	private final String val;

	DimensionUnit(String val) {
		this.val = val;
	}

	@Override
	public String toString() {
		return name().toLowerCase();
	}

	public String value(){
		return val;
	}


	public static DimensionUnit fromValue(String value) {
		for(DimensionUnit e: DimensionUnit.values()) {
			if(e.val.equalsIgnoreCase(value)) {
				return e;
			}
		}
		return null;// not found
	}

}
