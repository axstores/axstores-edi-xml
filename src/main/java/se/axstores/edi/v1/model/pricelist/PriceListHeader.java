package se.axstores.edi.v1.model.pricelist;

import se.axstores.edi.v1.model.common.ContactInfo;
import se.axstores.edi.v1.model.common.Period;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-pricelist-v1.0")
public class PriceListHeader {

    @XmlElement(required = true)
    private ContactInfo supplier;

    @XmlElement(required = true)
    private ContactInfo buyer;

    @XmlElement
    private String priceListName;

    @XmlElement(required = true)
    private Period priceListPeriod;

    @XmlElement
    private Period deliveryPeriod;

    @XmlElement
    private Period salesPeriod;

    /**
     * ISO 4217 currency codes
     */
    @XmlElement(required = true)
    private String defaultCurrency;

    /**
     * ISO 3166-1 country code, alpha-2 or alpha-3
     */
    @XmlElement(required = false)
    private String defaultCountry;

    /**
     * ISO 639-2 code, alpha-2 or alpha-3
     */
    @XmlElement(required = false)
    private String defaultLanguage;

    public ContactInfo getSupplier() {
        return supplier;
    }

    public void setSupplier(ContactInfo supplier) {
        this.supplier = supplier;
    }

    public ContactInfo getBuyer() {
        return buyer;
    }

    public void setBuyer(ContactInfo buyer) {
        this.buyer = buyer;
    }

    public String getPriceListName() {
        return priceListName;
    }

    public void setPriceListName(String priceListName) {
        this.priceListName = priceListName;
    }

    public Period getPriceListPeriod() {
        return priceListPeriod;
    }

    public void setPriceListPeriod(Period priceListPeriod) {
        this.priceListPeriod = priceListPeriod;
    }

    public Period getDeliveryPeriod() {
        return deliveryPeriod;
    }

    public void setDeliveryPeriod(Period deliveryPeriod) {
        this.deliveryPeriod = deliveryPeriod;
    }

    public Period getSalesPeriod() {
        return salesPeriod;
    }

    public void setSalesPeriod(Period salesPeriod) {
        this.salesPeriod = salesPeriod;
    }

    public String getDefaultCurrency() {
        return defaultCurrency;
    }

    public void setDefaultCurrency(String defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
    }

    public String getDefaultCountry() {
        return defaultCountry;
    }

    public void setDefaultCountry(String defaultCountry) {
        this.defaultCountry = defaultCountry;
    }

    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }
}
