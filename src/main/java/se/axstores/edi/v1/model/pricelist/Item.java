package se.axstores.edi.v1.model.pricelist;

import se.axstores.edi.v1.model.common.*;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * The Item
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Item", namespace = "uri:axstores-edi-xml:edi-pricelist-v1.0")
public class Item {

    /**
     * Optional line number id.
     * <p>
     * Maps from LIN, line item number
     */
    @XmlAttribute
    private Integer lineNr;

    /**
     * Id of the item
     * <p>
     * Maps from LIN, item number identification
     */
    @XmlElement(required = true)
    private Identifier id;

    /**
     * Vendor item number
     * <p>
     * Maps from PIA, SA
     */
    @XmlElement(required = true)
    private Identifier articleVpn;

    /**
     * Vendor item number
     * <p>
     * Maps from PIA, SK
     */
    @XmlElement(required = true)
    private Identifier itemVpn;

    /**
     * Barcode list Maps from
     */
    @XmlElementWrapper(name = "barcodeList", required = true)
    @XmlElement(name = "barcode")
    private List<Identifier> barcodeList;

    /**
     * Vendor provided HTS code.
     * <p>
     * Not in PRICAT
     */
    @XmlElement
    private TypedValue htsCode;

    /**
     * Information about sustainability.
     * <p>
     * Maps from PIA+1+FSC
     */
    @XmlElement
    private SustainabilityInfo sustainabilityInfo;

    /**
     * Vendor SKU
     * <p>
     * Maps from IMD, BRN
     */
    @XmlElement(required = true)
    private String brand;

    /**
     * Function
     * <p>
     * Maps from IMD, FNM
     */
    @XmlElement
    private String function;

    /**
     * Vendor series name
     * <p>
     * Maps from IMD, ANM
     */
    @XmlElement(required = false)
    private String series;

    /**
     * Vendor article name
     * <p>
     * Maps from IMD, ANM
     */
    @XmlElement(required = true)
    private String name;

    /**
     * Vendor color code and color name
     * <p>
     * Maps from IMD, code list 7081: color=35
     */
    @XmlElement
    private CodeValue vendorColor;

    /**
     * Vendor size
     * <p>
     * Size type indicates unit which sizing system used: Europ, US, UK &
     * Ireland,... See: https://en.wikipedia.org/wiki/Shoe_size
     */
    @XmlElement
    private TypedValue vendorSize;

    /**
     * Item measurements
     * <p>
     * Maps from MEA physical dimension
     */
    @XmlElement
    private Dimensions itemDimensions;

    /**
     * Item weight
     * <p>
     * NOt used
     */
    @XmlElement
    private Weight itemGrossWeight;

    /**
     * Item weight
     * <p>
     * NOt used
     */
    @XmlElement
    private Weight itemNetWeight;

    /**
     * Item packing info
     * <p>
     * Maps from QTY
     */
    @XmlElement
    private PackingInfo packingInfo;

    /**
     * Country of manufacturing.
     * <p>
     * Maps from ALI
     */
    @XmlElement
    private String countryOfManufacturing;

    /**
     * Period when item can be ordered
     */
    @XmlElement
    private Period orderPeriod;

    /**
     * Period when ordered item can be delivered.
     */
    @XmlElement
    private Period deliveryPeriod;

    /**
     * Period when item is allowed to be sold
     */
    @XmlElement
    private Period salesPeriod;

    /**
     * Period when item has been available, i.e. item lifetime.
     * <p>
     * Period end indicates endo-of-life of item.
     */
    @XmlElement
    private Period availabilityPeriod;

    /**
     * Vendor specified season or period name in coded format. E.g. F/W 2018,
     * S/S 2019
     */
    @XmlElement
    private String season;

    /**
     * Vendor specified phase (in the season) name in coded format. E.g. Autumn,
     * Christmas, Winter, Easter, Summer
     */
    @XmlElement
    private String phase;

    /**
     * Item cost from supplier, i.e. buyers purchase price
     * <p>
     * Maps from PRI+AAB, currency as specified for supplier
     * <p>
     * TODO: Should be list if we want to support multiple prices in same list.
     * - one regular price period for the price list period (or open ending) and
     * additional reduced prices within the normal price period.
     */
    @XmlElement
    private CostPriceInfo costPriceInfo;

    /**
     * Item VAT percent.
     */
    @XmlElement(required = true)
    private Integer vatPercent;

    // Hack below due to limitation in Excel XML mapping - see comment below!
    /**
     * Recommended retail prices from supplier, for different countries in
     * different currencies
     * <p>
     * Maps from PRI+INF + CUX+2
     */
    @XmlElement
    private RetailPriceInfo retailPriceInfoSEK;

    // Hack above due to limitation in Excel XML mapping
    // To be replaced with code below
    // @XmlElementWrapper(name = "retailPriceInfoList", required = true)
    // @XmlElement(name = "retailPriceInfo")
    // private List<RetailPriceInfo> retailPriceInfoList;

    @XmlElementWrapper(name = "materialInfoList")
    @XmlElement
    private List<MaterialInfo> materialInfo;

    /**
     * Transport and handoing information for hazardous goods
     * <p>
     * Maps from HAN
     */
    @XmlElement
    private HazardousGoodsInfo hazardousGoodsInfo;

    //
    // Dummy elements needed in Excel mapping table = "HACK"
    //
    @XmlElement
    private String dummy1;
    @XmlElement
    private String dummy2;
    @XmlElement
    private String dummy3;
    @XmlElement
    private String dummy4;
    @XmlElement
    private String dummy5;
    @XmlElement
    private String dummy6;
    @XmlElement
    private String dummy7;
    @XmlElement
    private String dummy8;
    @XmlElement
    private String dummy9;
    @XmlElement
    private String dummy10;
    @XmlElement
    private String dummy11;
    @XmlElement
    private String dummy12;
    @XmlElement
    private String dummy13;
    @XmlElement
    private String dummy14;
    @XmlElement
    private String dummy15;


    public Integer getLineNr() {
        return lineNr;
    }

    public void setLineNr(Integer lineNr) {
        this.lineNr = lineNr;
    }

    public Identifier getId() {
        return id;
    }

    public void setId(Identifier id) {
        this.id = id;
    }

    public Identifier getArticleVpn() {
        return articleVpn;
    }

    public void setArticleVpn(Identifier articleVpn) {
        this.articleVpn = articleVpn;
    }

    public Identifier getItemVpn() {
        return itemVpn;
    }

    public void setItemVpn(Identifier itemVpn) {
        this.itemVpn = itemVpn;
    }

    public List<Identifier> getBarcodeList() {
        return barcodeList;
    }

    public void setBarcodeList(List<Identifier> barcodeList) {
        this.barcodeList = barcodeList;
    }

    public TypedValue getHtsCode() {
        return htsCode;
    }

    public void setHtsCode(TypedValue htsCode) {
        this.htsCode = htsCode;
    }

    public SustainabilityInfo getSustainabilityInfo() {
        return sustainabilityInfo;
    }

    public void setSustainabilityInfo(SustainabilityInfo sustainabilityInfo) {
        this.sustainabilityInfo = sustainabilityInfo;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CodeValue getVendorColor() {
        return vendorColor;
    }

    public void setVendorColor(CodeValue vendorColor) {
        this.vendorColor = vendorColor;
    }

    public TypedValue getVendorSize() {
        return vendorSize;
    }

    public void setVendorSize(TypedValue vendorSize) {
        this.vendorSize = vendorSize;
    }

    public Dimensions getItemDimensions() {
        return itemDimensions;
    }

    public void setItemDimensions(Dimensions itemDimensions) {
        this.itemDimensions = itemDimensions;
    }

    public Weight getItemGrossWeight() {
        return itemGrossWeight;
    }

    public void setItemGrossWeight(Weight itemGrossWeight) {
        this.itemGrossWeight = itemGrossWeight;
    }

    public Weight getItemNetWeight() {
        return itemNetWeight;
    }

    public void setItemNetWeight(Weight itemNetWeight) {
        this.itemNetWeight = itemNetWeight;
    }

    public PackingInfo getPackingInfo() {
        return packingInfo;
    }

    public void setPackingInfo(PackingInfo packingInfo) {
        this.packingInfo = packingInfo;
    }

    public String getCountryOfManufacturing() {
        return countryOfManufacturing;
    }

    public void setCountryOfManufacturing(String countryOfManufacturing) {
        this.countryOfManufacturing = countryOfManufacturing;
    }

    public Period getOrderPeriod() {
        return orderPeriod;
    }

    public void setOrderPeriod(Period orderPeriod) {
        this.orderPeriod = orderPeriod;
    }

    public Period getDeliveryPeriod() {
        return deliveryPeriod;
    }

    public void setDeliveryPeriod(Period deliveryPeriod) {
        this.deliveryPeriod = deliveryPeriod;
    }

    public Period getSalesPeriod() {
        return salesPeriod;
    }

    public void setSalesPeriod(Period salesPeriod) {
        this.salesPeriod = salesPeriod;
    }

    public Period getAvailabilityPeriod() {
        return availabilityPeriod;
    }

    public void setAvailabilityPeriod(Period availabilityPeriod) {
        this.availabilityPeriod = availabilityPeriod;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public CostPriceInfo getCostPriceInfo() {
        return costPriceInfo;
    }

    public void setCostPriceInfo(CostPriceInfo costPriceInfo) {
        this.costPriceInfo = costPriceInfo;
    }

    public Integer getVatPercent() {
        return vatPercent;
    }

    public void setVatPercent(Integer vatPercent) {
        this.vatPercent = vatPercent;
    }

    public RetailPriceInfo getRetailPriceInfoSEK() {
        return retailPriceInfoSEK;
    }

    public void setRetailPriceInfoSEK(RetailPriceInfo retailPriceInfoSEK) {
        this.retailPriceInfoSEK = retailPriceInfoSEK;
    }

    public List<MaterialInfo> getMaterialInfo() {
        return materialInfo;
    }

    public void setMaterialInfo(List<MaterialInfo> materialInfo) {
        this.materialInfo = materialInfo;
    }

    public HazardousGoodsInfo getHazardousGoodsInfo() {
        return hazardousGoodsInfo;
    }

    public void setHazardousGoodsInfo(HazardousGoodsInfo hazardousGoodsInfo) {
        this.hazardousGoodsInfo = hazardousGoodsInfo;
    }

    public String getDummy1() {
        return dummy1;
    }

    public void setDummy1(String dummy1) {
        this.dummy1 = dummy1;
    }

    public String getDummy2() {
        return dummy2;
    }

    public void setDummy2(String dummy2) {
        this.dummy2 = dummy2;
    }

    public String getDummy3() {
        return dummy3;
    }

    public void setDummy3(String dummy3) {
        this.dummy3 = dummy3;
    }

    public String getDummy4() {
        return dummy4;
    }

    public void setDummy4(String dummy4) {
        this.dummy4 = dummy4;
    }

    public String getDummy5() {
        return dummy5;
    }

    public void setDummy5(String dummy5) {
        this.dummy5 = dummy5;
    }

    public String getDummy6() {
        return dummy6;
    }

    public void setDummy6(String dummy6) {
        this.dummy6 = dummy6;
    }

    public String getDummy7() {
        return dummy7;
    }

    public void setDummy7(String dummy7) {
        this.dummy7 = dummy7;
    }

    public String getDummy8() {
        return dummy8;
    }

    public void setDummy8(String dummy8) {
        this.dummy8 = dummy8;
    }

    public String getDummy9() {
        return dummy9;
    }

    public void setDummy9(String dummy9) {
        this.dummy9 = dummy9;
    }

    public String getDummy10() {
        return dummy10;
    }

    public void setDummy10(String dummy10) {
        this.dummy10 = dummy10;
    }

    public String getDummy11() {
        return dummy11;
    }

    public void setDummy11(String dummy11) {
        this.dummy11 = dummy11;
    }

    public String getDummy12() {
        return dummy12;
    }

    public void setDummy12(String dummy12) {
        this.dummy12 = dummy12;
    }

    public String getDummy13() {
        return dummy13;
    }

    public void setDummy13(String dummy13) {
        this.dummy13 = dummy13;
    }

    public String getDummy14() {
        return dummy14;
    }

    public void setDummy14(String dummy14) {
        this.dummy14 = dummy14;
    }

    public String getDummy15() {
        return dummy15;
    }

    public void setDummy15(String dummy15) {
        this.dummy15 = dummy15;
    }
}
