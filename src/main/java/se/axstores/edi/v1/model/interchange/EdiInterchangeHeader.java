package se.axstores.edi.v1.model.interchange;

import se.axstores.edi.util.xmladapter.IsoDateAdapter;
import se.axstores.edi.v1.model.common.ContactInfo;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-interchange-v1.0")
public class EdiInterchangeHeader {
    /**
     * Version of the interchange format specification. Mapps to EDIFACT UNB
     * segment info.
     */
    @XmlAttribute(required = true)
    private String version;

    /**
     * Senders message id. Maps to EDIFACT UNB
     */
    @XmlElement(required = true)
    private String messageId;

    /**
     * Senders message creation date. Maps to EDIFACT UNB
     */
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(IsoDateAdapter.class)
    private LocalDate date;

    /**
     * Sender identifier
     */
    @XmlElement(required = true)
    private ContactInfo sender;

    /**
     * Receiver/recipient identifier
     */
    @XmlElement(required = true)
    private ContactInfo recipient;

    /**
     * Recievers reference
     */
    @XmlElement
    private String reference;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public ContactInfo getSender() {
        return sender;
    }

    public void setSender(ContactInfo sender) {
        this.sender = sender;
    }

    public ContactInfo getRecipient() {
        return recipient;
    }

    public void setRecipient(ContactInfo recipient) {
        this.recipient = recipient;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

}
