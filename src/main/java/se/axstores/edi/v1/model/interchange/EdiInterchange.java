package se.axstores.edi.v1.model.interchange;

import se.axstores.edi.v1.model.common.EdiMessage;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Interchange information - information of sending and receiving parties
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(namespace = "uri:axstores-edi-xml:edi-interchange-v1.0")
@XmlType(namespace = "uri:axstores-edi-xml:edi-interchange-v1.0")
public class EdiInterchange {

    /**
     * Interchange information
     */
    @XmlElement(required = true)
    private EdiInterchangeHeader header;

    /**
     * List of EdiMessages
     */
    @XmlElementWrapper(name = "messageList")
    @XmlAnyElement(lax = true)
    private List<? extends EdiMessage> messages;

    public EdiInterchangeHeader getHeader() {
        return header;
    }

    public void setHeader(EdiInterchangeHeader header) {
        this.header = header;
    }

    public List<? extends EdiMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<? extends EdiMessage> messages) {
        this.messages = messages;
    }
}
