package se.axstores.edi.v1.model.common;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "uri:axstores-edi-xml:edi-common-v1.0")
public class Barcode {

    @XmlAttribute(required = true)
    private BarcodeUnit type;

    @XmlValue
    private String value;

    public BarcodeUnit getType() {
        return type;
    }

    public void setType(BarcodeUnit type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
