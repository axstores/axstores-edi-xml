# Excel HOWTO #

# Overview
Describes how to work with the Excel files that import the XML data files.

## How to load data from a file
 1. Open on of the Excel files in the 'excel-files' directory.
 2. Right-click on one of the cells in the table - this will show the context menu.
   * Select XML -> Import 
   		- locate the XML file to import and open it.

   
## Known problems to check if no data is loaded.
  * Is the xmlns in the XML file correct? 
  * ... TBD ...
 


