# README #

Axstores internal EDI XML format.

### What is this repository for? ###

* Java files and JABX generated XSD/XMLD defines the data model
* The excel files (in directory excel-files) defines the data import of the XML to a spreadsheet.

### Build and package

Make sure `mvn`, `java`, `bash` and `zip` commands are on the PATH. 
E.g.,`%USERPROFILE%\Development\setenv.bat` or `~/Development/setenv.sh`

#### Build

Build and package all files with the following command:

```bash
$> mvn clean package
```

#### Create and package zip files
Assume bash is available
```
$> bin/create-specification-pack.sh
```

```
$> bin/update-massreg-excel-xsd-and-xml.sh
```

```
$> bin/create-excel-pack.sh
```
## TODO
* Highlight selected system and integrations (different colors)
* Group systmes
* Size graph area + put border on area
* download CSV
* Info area on left
* Edge popu description looks strange '::'
* zoom and zoom out

